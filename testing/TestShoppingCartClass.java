import org.junit.BeforeClass;
import org.junit.Test;
import shop.MobilePhone;
import shop.ShoppingCart;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class TestShoppingCartClass {

    private static ShoppingCart cart;
    private static MobilePhone mobilePhone;

    @BeforeClass
    public static void setupPhoneAndCart(){
        cart = new ShoppingCart();
        mobilePhone = new MobilePhone(1, "testPhone", "Phone for Test", "TestManufact..", 23.56);
        mobilePhone.setMobilePhone(5.3, 12, 8, 2575, "TestOS", 13);
    }

    @Test
    public void productIsAddedToCart(){
        cart.add(mobilePhone);

        assertEquals(mobilePhone, cart.getProducts().get(0));
        assertEquals(23.56, cart.getTotalPrice(), 0);
    }

    @Test
    public void productIsRemovedFromCart(){
        cart.remove(0);

        assertEquals(0, cart.getProducts().size());
        assertEquals(0, cart.getTotalPrice(), 0);
    }

    @Test
    public void shoppingCartShouldBeSubmitted(){
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outStream));

        cart.add(mobilePhone);
        cart.submit();

        String output = "Shopping Cart submitted\r\n"
        +"***************************************************************\r\n"
        +"Your Invoice\r\n"
        +"TestManufact.. testPhone                  23.56\r\n"
        +"**************************************\r\n"
        +"Total Price:                              23.56\n\n\r\n"
        +"Thank you for your Purchase\r\n"
        +"***************************************************************\r\n";

        assertEquals(output, outStream.toString());
        assertEquals(0, cart.getProducts().size());

        System.setOut(null);
    }

    @Test
    public void purchaseShouldBeCanceled(){
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outStream));
        cart.add(mobilePhone);
        cart.cancel();

        assertEquals("Canceled your purchase!\r\n", outStream.toString());
        assertEquals(0, cart.getProducts().size());

        System.setOut(null);
    }
}
