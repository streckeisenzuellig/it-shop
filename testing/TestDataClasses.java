import org.junit.Test;
import shop.DesktopPc;
import shop.MobilePhone;
import shop.Notebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class TestDataClasses {

    //these tests check the constructors of the data classes

    @Test
    public void mobilePhoneShouldBeCreatedCorrect() {
        MobilePhone mobilePhone = new MobilePhone(1, "testPhone", "Phone for Test", "TestManufact..", 23.56);
        mobilePhone.setMobilePhone(5.3, 12, 8, 2575, "TestOS", 13);

        assertEquals(1, mobilePhone.getProductID());
        assertEquals("testPhone", mobilePhone.getProductName());
        assertEquals("Phone for Test", mobilePhone.getDescription());
        assertEquals("TestManufact..", mobilePhone.getManufacturer());
        assertEquals(23.56, mobilePhone.getPrice(), 0);

        assertEquals(5.3, mobilePhone.getScreenSize(), 0);
        assertEquals( 12, mobilePhone.getCameraResolution(), 0);
        assertEquals(8, mobilePhone.getRamSize());
        assertEquals(2575, mobilePhone.getBatteryCapacity(), 0);
        assertEquals("TestOS", mobilePhone.getOperatingSystem());
        assertEquals(13, mobilePhone.getSpace());
    }

    @Test
    public void notebookShouldBeCreatedCorrect() {
        Notebook notebook = new Notebook(2, "testNotebook", "Notebook for Test", "TestManufact..", 489.23);
        notebook.setComputerProperties("Test cpu", "Test graphicCard", "TestOS", 45, 380);
        notebook.setNotebookProperties(23.5, 2357.21, false, 35.43, 5693);

        assertEquals(2, notebook.getProductID());
        assertEquals("testNotebook", notebook.getProductName());
        assertEquals("Notebook for Test", notebook.getDescription());
        assertEquals("TestManufact..", notebook.getManufacturer());
        assertEquals(489.23, notebook.getPrice(), 0);

        assertEquals("Test cpu", notebook.getCpu());
        assertEquals("Test graphicCard", notebook.getGraphicCard());
        assertEquals("TestOS", notebook.getOperatingSystem());
        assertEquals(45, notebook.getRamSize());
        assertEquals(380, notebook.getSsdSpace());

        assertEquals(23.5, notebook.getScreenSize(), 0);
        assertEquals(2357.21, notebook.getWeight(), 0);
        assertFalse(notebook.hasTouchScreen());
        assertEquals(35.43, notebook.getCameraResolution(), 0);
        assertEquals(5693, notebook.getBatteryCapacity());
    }

    @Test
    public void desktopPcShouldBeCreatedCorrect() {
        DesktopPc desktopPc = new DesktopPc(3, "testDesktop", "Desktop for Test", "TestManufact..", 765.43, 1234);
        desktopPc.setComputerProperties("Test cpu2", "Test graphicCard2", "TestOS2", 452, 3802);

        assertEquals(3, desktopPc.getProductID());
        assertEquals("testDesktop", desktopPc.getProductName());
        assertEquals("Desktop for Test", desktopPc.getDescription());
        assertEquals("TestManufact..", desktopPc.getManufacturer());
        assertEquals(765.43, desktopPc.getPrice(), 0);

        assertEquals("Test cpu2", desktopPc.getCpu());
        assertEquals("Test graphicCard2", desktopPc.getGraphicCard());
        assertEquals("TestOS2", desktopPc.getOperatingSystem());
        assertEquals(452, desktopPc.getRamSize());
        assertEquals(3802, desktopPc.getSsdSpace());

        assertEquals(1234, desktopPc.getHddSpace());
    }

}
