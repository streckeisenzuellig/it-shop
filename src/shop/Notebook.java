package shop;

public class Notebook extends Computer {

    //Declaration of Variables
    private double screenSize;
    private double weight;
    private boolean hasTouchScreen;
    private double cameraResolution;
    private int batteryCapacity;


    //Constructor
    public Notebook(int productID, String productName, String description, String manufacturer, double price) {
        super(productID, productName, description, manufacturer, price);
    }


    //Getters
    public double getScreenSize() {
        return screenSize;
    }

    public double getWeight() {
        return weight;
    }

    public boolean hasTouchScreen() {
        return hasTouchScreen;
    }

    public double getCameraResolution() {
        return cameraResolution;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    //Setter

    public void setNotebookProperties(double screenSize, double weight, boolean hasTouchScreen, double cameraResolution, int batteryCapacity){
        this.screenSize = screenSize;
        this.weight = weight;
        this.hasTouchScreen = hasTouchScreen;
        this.cameraResolution = cameraResolution;
        this.batteryCapacity = batteryCapacity;
    }
}
