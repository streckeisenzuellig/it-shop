package shop;

public interface IShoppingService {

    //add a product to the shopping cart
    public abstract void add(Product product);

    //remove a product from the shopping cart
    public abstract void remove(int index);

    //content of shopping cart is final, accounting starts
    public abstract void submit();

    //cancels the accounting process and clears the shopping cart
    public abstract void cancel();

}
