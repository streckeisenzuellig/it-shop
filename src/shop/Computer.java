package shop;

public abstract class Computer extends Product {

    //Declaration of Variables
    private String cpu;
    private int ramSize;
    private String operatingSystem;
    private int ssdSpace;
    private String graphicCard;


    //Constructor
    public Computer(int productID, String productName, String description, String manufacturer, double price){
        super(productID, productName, description, manufacturer, price);
    }

    //Getters
    public String getCpu() {
        return cpu;
    }

    public int getRamSize() {
        return ramSize;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public int getSsdSpace() {
        return ssdSpace;
    }

    public String getGraphicCard() {
        return graphicCard;
    }


    //Setters
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public void setRamSize(int ramSize) {
        this.ramSize = ramSize;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public void setSsdSpace(int ssdSpace) {
        this.ssdSpace = ssdSpace;
    }

    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }

    public void setComputerProperties(String cpu, String graphicCard, String operatingSystem, int ramSize, int ssdSpace){
        setCpu(cpu);
        setGraphicCard(graphicCard);
        setOperatingSystem(operatingSystem);
        setRamSize(ramSize);
        setSsdSpace(ssdSpace);
    }
}
