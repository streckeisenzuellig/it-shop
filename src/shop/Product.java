package shop;

public abstract class Product {

    //Declaration of Variables
    private int productId;
    private String productName;
    private String description;
    private String manufacturer;
    private double price;

    //Constructor
    public Product(int productId, String productName, String description, String manufacturer, double price){
        this.productId = productId;
        this.productName = productName;
        this.description = description;
        this.manufacturer = manufacturer;
        this.price = price;
    }



    //Getters

    public int getProductID() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getDescription() {
        return description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString(){
        return this.productName;
    }
}
