package shop;

public class DesktopPc extends Computer {

    //Declaration of Variables
    private int hddSpace;

    //Constructor
    public DesktopPc(int productID, String productName, String description, String manufacturer, double price, int hddSpace){
        super(productID, productName, description, manufacturer, price);

        this.hddSpace = hddSpace;
    }

    //Getter
    public int getHddSpace() {
        return hddSpace;
    }
}
