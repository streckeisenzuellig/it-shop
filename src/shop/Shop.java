package shop;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Shop {

    //Declaration of Variables
    private Scanner scanner;
    private ArrayList<MobilePhone> mobilePhones;
    private ArrayList<DesktopPc> desktopPcs;
    private ArrayList<Notebook> notebooks;

    public String mobilesFile = "Products/mobilephones.json";
    public String desktopsFile = "Products/desktopPcs.json";
    public String notebooksFile = "Products/notebooks.json";
    private ShoppingCart shoppingCart;

    //Constructor
    public Shop(){
        scanner = new Scanner(System.in);
        getMobilePhonesFromFile();
        getDesktopPcsFromFile();
        getNotebooksFromFile();
        shoppingCart = new ShoppingCart();
    }

    //parses mobilePhones and saves them to mobilePhone arrayList
    private boolean getMobilePhonesFromFile(){
        try{
            Gson gson = new Gson();
            mobilePhones = gson.fromJson(new BufferedReader(new FileReader(mobilesFile)), new TypeToken<ArrayList<MobilePhone>>(){}.getType());
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    //parses desktopPcs and saves them to desktopPc arrayList
    private boolean getDesktopPcsFromFile(){
        try {
            Gson gson = new Gson();
            desktopPcs = gson.fromJson(new BufferedReader(new FileReader(desktopsFile)), new TypeToken<ArrayList<DesktopPc>>(){}.getType());
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    //parses notebooks and saves them to notebook arrayList
    private boolean getNotebooksFromFile(){
        try {
            Gson gson = new Gson();
            notebooks = gson.fromJson(new BufferedReader(new FileReader(notebooksFile)), new TypeToken<ArrayList<Notebook>>(){}.getType());
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    //prints menu (product categories) (called from main)
    public void printMenu(){
        System.out.println("****************************************");
        System.out.println("*       Welcome to the It-Shop         *");
        System.out.println("****************************************");
        System.out.println("*                                      *");
        System.out.println("*   Press <0> to exit program          *");
        System.out.println("*   Press <1> to get to DesktopPCs     *");
        System.out.println("*   Press <2> to get to Notebooks      *");
        System.out.println("*   Press <3> to get to Phones         *");
        System.out.println("*                                      *");
        System.out.println("*                                      *");
        System.out.println("****************************************");
        int productTypeId = scanner.nextInt();

        if (productTypeId == 0){
            System.exit(0);
        }
        else {
            printProducts(productTypeId);
        }
    }

    //prints all products form selected category (called from printMenu)
    public void printProducts(int productTypeId){
        ArrayList productsArrayList = new ArrayList();

        switch (productTypeId){
            case 1:
                desktopPcs = getDesktopPcs();
                int index = 1;
                System.out.println("****************************************");
                for (DesktopPc product: desktopPcs
                        ) { System.out.println("Press<"+index+"> for "+product.toString());
                    index++;
                }
                System.out.println("****************************************");
                break;
            case 2:
                notebooks = getNotebooks();
                int indexn = 1;
                System.out.println("****************************************");
                for (Notebook product: notebooks
                        ) { System.out.println("Press<"+indexn+"> for "+product.toString());
                    indexn++;
                }
                System.out.println("****************************************");
                break;
            case 3:
                mobilePhones = getMobilePhones();
                int indexm = 1;
                System.out.println("****************************************");
                for (MobilePhone product: mobilePhones
                        ) { System.out.println("Press<"+indexm+"> for "+product.toString());
                    indexm++;
                }
                System.out.println("****************************************");
                break;
             default:
                System.out.println("****************************************");
                System.out.println("*          Ungültige Eingabe           *");
                System.out.println("****************************************");
                printMenu();
                return;
        }
        System.out.println("Press <0> to go back");
        System.out.println("****************************************");

        int selectedProductId = scanner.nextInt();

        if (selectedProductId == 0) {
            printMenu();
        }
        else {
            printProductInfo(productTypeId, selectedProductId);
        }
    }

    //prints all properties of product (productTypeId is given from printProducts())
    public void printProductInfo(int productTypeId, int productId){
        Product product;

        System.out.println("****************************************");
        switch (productTypeId){
            case 1:
                DesktopPc pc = null;
                for (DesktopPc p: desktopPcs
                     ) {if(p.getProductID() == productId)
                         pc = p;
                }

                System.out.println("* Hersteller: " +pc.getManufacturer()+          "        *");
                System.out.println("* Name:       " +pc.getProductName()+          "         *");
                System.out.println("* HDD:        " +pc.getHddSpace()+           "           *");
                System.out.println("* SSD:        " +pc.getSsdSpace()+           "           *");
                System.out.println("* Price:      " +pc.getPrice()+           "              *");
                product = pc;
                break;
            case 2:
                Notebook notebook = null;
                for (Notebook nb: notebooks
                        ) {if(nb.getProductID() == productId)
                    notebook = nb;
                }

                System.out.println("* Hersteller: " +notebook.getManufacturer()+          "        *");
                System.out.println("* Name:       " +notebook.getProductName()+          "         *");
                System.out.println("* OS:        " +notebook.getOperatingSystem()+           "           *");
                System.out.println("* SSD:        " +notebook.getSsdSpace()+           "           *");
                System.out.println("* Price:      " +notebook.getPrice()+           "              *");
                product = notebook;
                break;
            case 3:
                MobilePhone phone = null;
                for (MobilePhone ph: mobilePhones
                        ) {if(ph.getProductID() == productId)
                    phone = ph;
                }

                System.out.println("* Hersteller: "+phone.getManufacturer()+          "        *");
                System.out.println("* Name:       " +phone.getProductName()+          "         *");
                System.out.println("* OS:         " +phone.getOperatingSystem()+           "     *");
                System.out.println("* Screen      " +phone.getScreenSize()+           "           *");
                System.out.println("* Price:      " +phone.getPrice()+           "              *");
                product = phone;
                break;
            default:
                System.out.println("****************************************");
                System.out.println("*          Ungültige Eingabe           *");
                System.out.println("****************************************");
                printMenu();
                return;
        }
        System.out.println("****************************************");

        printActionMenu(product, productTypeId);
    }

    //prints to select what to do with the selected product
    public void printActionMenu(Product product, int productTypeId){
        System.out.println("****************************************");
        System.out.println("*   Press <1> to add Product to Shopping Cart           *");
        System.out.println("*   Press <2> to go to ShoppingCart           *");
        System.out.println("*   Press <3> to go Back           *");
        System.out.println("****************************************");

        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input){
            case 1:
                shoppingCart.add(product);
                break;
            case 2:
                shoppingCart.printShoppingCart();
                break;
            case 3:
                printProducts(productTypeId);
                break;
            default:
                System.out.println("****************************************");
                System.out.println("*          Ungültige Eingabe           *");
                System.out.println("****************************************");
        }

        printActionMenu(product, productTypeId);

    }

    //Getters

    public ArrayList<MobilePhone> getMobilePhones() {
        return mobilePhones;
    }

    public ArrayList<DesktopPc> getDesktopPcs() {
        return desktopPcs;
    }

    public ArrayList<Notebook> getNotebooks() {
        return notebooks;
    }
}
