package shop;

import java.util.ArrayList;
import java.util.Scanner;

public class ShoppingCart implements IShoppingService {

    //Declaration of Variables
    private ArrayList<Product> products;
    private double totalPrice;

    //Constructor
    public ShoppingCart(){
        products = new ArrayList<>();
        totalPrice = 0;
    }

    //calculates price of all products in arrayList
    public void calculateTotalPrice(){
        totalPrice = 0;

        for (Product product: products){
            totalPrice += product.getPrice();
        }
    }

    //prints out all the items in the shopping cart
    public void printShoppingCart(){
        System.out.println("****************************************");
        System.out.println("             Your ShoppingCart          ");
        System.out.println("****************************************");
        for (int i = 0; i < products.size(); i++){
            System.out.println("Nr: " + i + " " + products.get(i).getManufacturer() + " " + products.get(i).getProductName());
        }
        System.out.println("****************************************");

        printShoppingCartOptions();
    }

    //prints out the possible interaction you can do with the items in the shopping cart
    public void printShoppingCartOptions(){
        System.out.println("****************************************");
        System.out.println("*   Press <1> to remove Product from Shopping Cart           *");
        System.out.println("*   Press <2> to delete ShoppingCart           *");
        System.out.println("*   Press <3> to submit ShoppingCart           *");
        System.out.println("*   Press <4> to exit           *");
        System.out.println("****************************************");

        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input){
            case 1:
                System.out.println("Type Nr of Product to remove: ");
                int remove = scanner.nextInt();
                remove(remove);
                printShoppingCartOptions();
                break;
            case 2:
                cancel();
                break;
            case 3:
                submit();
                break;
            case 4:
                System.exit(0);
        }
    }

    @Override
    public void add(Product product) {
        products.add(product);
        calculateTotalPrice();
    }

    @Override
    public void remove(int index) {
        products.remove(index);
        calculateTotalPrice();
    }

    @Override
    public void submit() {
        System.out.println("Shopping Cart submitted");
        printInvoice();
        products.clear();
    }

    @Override
    public void cancel() {
        System.out.println("Canceled your purchase!");
        products.clear();
    }

    //prints out your bill
    private void printInvoice(){
        System.out.println("***************************************************************");
        System.out.println("Your Invoice");
        for (Product product: products){
            System.out.println(product.getManufacturer() + " " + product.getProductName() + "                  " + product.getPrice());
        }
        System.out.println("**************************************");
        System.out.println("Total Price:                              " + totalPrice + "\n\n");
        System.out.println("Thank you for your Purchase");
        System.out.println("***************************************************************");
    }

    //Getters
    public double getTotalPrice() {
        return totalPrice;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

}
