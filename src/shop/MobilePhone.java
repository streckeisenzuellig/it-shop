package shop;

public class MobilePhone extends Product {

    //Declaration of Variable
    private double screenSize;
    private double cameraResolution;
    private int ramSize;
    private int batteryCapacity;
    private String operatingSystem;
    private int space;

    //Constructor
    public MobilePhone(int productId, String productName, String description, String manufacturer, double price){
        super(productId, productName, description, manufacturer, price);
    }

    //Getters
    public double getScreenSize() {
        return screenSize;
    }

    public double getCameraResolution() {
        return cameraResolution;
    }

    public int getRamSize() {
        return ramSize;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public int getSpace() { return space; }

    public void setMobilePhone(double screenSize, double cameraResolution, int ramSize, int batteryCapacity, String operatingSystem, int space){
        this.screenSize = screenSize;
        this.cameraResolution = cameraResolution;
        this.ramSize = ramSize;
        this.batteryCapacity = batteryCapacity;
        this.operatingSystem = operatingSystem;
        this.space = space;
    }
}
